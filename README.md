# GRPC example code for text cleaner

## Requires:
* GRPC
* Python3 (Server Side) I realize this seems silly but its to incorporate all the tools that people wonderfully build in python back into more useful languages
* GoLang (Client Side) This could be any language, but I like go.
* make
* spacy + whatever models... right now just:

        python -m spacy download en_core_web_lg

* pyspark 
* spark-nlp
* textblob

        python -m textblob.download_corpora

* I'm sure there may be others... Sorry.

## Build:
* cd rpc 
* make
* make run (executes grpc server, builds and runs client, terminates server) <- a short test
* make clean (Remove Generated content)
* make benchmark <- run a multicore benchmark to determine appropriate worker thread settings (estimate at 2/3 of available cores for server and client, though some implementations do not benefit as much as others (or at all))

## Interface:   

The current server has an interface of the type

```
   RPC_Object := &pb.Input{
            InputFormat: int32 0(LIST) or 1(STRING), 
            OutputFormat: int32 0(LIST) or 1(STRING), 
            Transformations: []int32, //as defined below
            StringIn: "", 
            StringListIn: []string
            }
```
    
### Available Transformations:

    0. To Uppercase  
    1. To Lowercase  
    2. To List -- Turns a string into a list of natural split tokens .split()
    3. To String -- Turns a list into a joined string of the type " ".join(list)
    4. Removes the slang acronyms from a text object. LOL -> Laughing out loud (NOTE mixed case is created by the dictionary if upper or lower is requred call that after)
    5. Spacy tokenizer -- Turns an input string into a list of strings tokenized by spacy (NOTE library is loaded only on first call, which makes that call slow)
    6. Spacy lemmatizer -- lematizes the input using spacy model
    7. Viper -- randomly injects visually similar characters into the input
        note that the Viper transformation is based of of https://github.com/UKPLab/naacl2019-like-humans-visual-attacks/tree/master/code/VIPER DCES model
    8. Spark Sentiment - returns a list of sentiment analysis strings ie ['positive', 'negative'...] for each sentence in the input. 
        REQUIRES String -> List
    9. Spark Spelling - Uses spark model to repair misspelled words
    10. Spacy Named Entities - Uses spacy to return named entities as a list with types
#### NOTE Text blob transformations are worse, but they are fast
    11. Text Blob Sentiment - Returns a single sentiment value tuple for a given group of words, DOES NOT BREAK INTO SENTENCES 
    12. Text Blob POS Tagger - Returns POS tags as a string
    13. Text Blob Spelling - Returns spelling corrected text. 70% claimed accuracy, very poor with named entities.
    14. Text blob lemmatizer - returns lematized text
    15. Text Blob Singularize - turns plural words into singlulars for the entire phrase.
