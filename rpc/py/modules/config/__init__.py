import yaml
conf = None
AcronymDictionaryLoaded = False
spacy_nlp = None
viperDict = None
viperProbability = 0.1
spark_init = False
spark_sentiment = None
spark_spelling = None


def Load(configFile):
    with open(configFile, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

