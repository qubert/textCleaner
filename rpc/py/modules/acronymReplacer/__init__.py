import json

DICTIONARY = {}

def Load(fileName):
    global DICTIONARY
    if len(DICTIONARY) == 0:
        with open(fileName) as slangDict:  
            DICTIONARY = json.load(slangDict)
    return True

def TranslateAcromymsFromString(s):
    return TranslateAcromymsFromList(s.split())

def TranslateAcromymsFromList(listOfTokens):
    global DICTIONARY
    for i, x in enumerate(listOfTokens):
        #TODO im not sure if this should actually be here... the dict is all in lower, but it returns some uppers, so it might be confusing if you run lower, then dict, then need to run lower again.  
        x = x.lower()
        if x in DICTIONARY:
            listOfTokens[i] = DICTIONARY[x]
    return " ".join(listOfTokens).split()




