#!/usr/bin/env python3
from concurrent import futures
from enum import Enum
from threading import Lock
from threading import current_thread
from threading import local
import time
import logging
import os
import random
import json

import grpc
import modules.config as config
import modules.acronymReplacer as acronyms
import transform_pb2
import transform_pb2_grpc

# NLP toolkit libs
import spacy

import sparknlp
from sparknlp.pretrained import PretrainedPipeline

from textblob import TextBlob
from textblob import Word

#this comes from the example code, its just used for the delay, not super important
_ONE_DAY_IN_SECONDS = 60 * 60 * 24

scriptDir = os.path.dirname(__file__)

# sharable models and libs
spacy_lock = Lock()
viper_lock = Lock()
spark_lock = Lock()

## individual spark pipelines.
spark_sentiment_lock = Lock()
spark_spelling_lock = Lock()
spark_ner_lock = Lock()

def toUpper(var):
    if isinstance(var, str):
        return var.upper()
    else:
        for i, x in enumerate(var):
            var[i] = x.upper()
        return var

def toLower(var):
    if isinstance(var, str):
        return var.lower()
    else:
        for i, x in enumerate(var):
            var[i] = x.lower()
        return var

def toList(var):
    return var.split()

def toString(var):
    return " ".join(var)

class Transformer(transform_pb2_grpc.TransformerServicer):
    def __init__(self):
        self.localData = local()

    def Transform(self, request, context):

        #  localData = workingDefinitions(request)
        self.localData.InputFormat = request.InputFormat
        self.localData.OutputFormat = request.OutputFormat
        self.localData.StringIn = request.StringIn
        self.localData.StringListIn = request.StringListIn
        self.localData.Error = ""

        #This is the switch statement that rolls through the emums
        for x in request.Transformations:
            if x == 0: # TO UPPER CASE
                self.localData.StringIn = toUpper(self.localData.StringIn)
                self.localData.StringListIn =toUpper(self.localData.StringListIn)
            elif x == 1: # To lower case
                self.localData.StringIn = toLower(self.localData.StringIn)
                self.localData.StringListIn =toLower(self.localData.StringListIn)
            # This wouself.localData be strictly a time saver, as there is an output type check prior to the return
            elif x == 2: # changes the Input format to a list.
                self._inputToList()
            elif x == 3: # changes the Input format to a string
                self._inputToString()
            elif x == 4: ## Transform Acroyms (LOL -> Laughing Out Loud)
                self._replaceAcronyms()
            elif x == 5: 
                self._spacySplit()
            elif x == 6:
                self._spacyLemmatize()
            elif x == 7:
                self._viper()
            elif x == 8:
                self._spark_sentiment()
            elif x == 9:
                self._spark_spelling()
            elif x == 10:
                self._spacyNER()
            elif x == 11:
                self._textBlobSentiment()
            elif x == 12:
                self._textBlobPOSTagging()
            elif x == 13:
                self._textBlobSpelling()
            elif x == 14:
                self._textBlobLemmatize()
            elif x == 15:
                self._textBlobSingularize()

        ## verify the output format is correct before returning
        if self._amString() and self._wantList():
            self._inputToList()
        elif self._amList()and self._wantString():
            self._inputToString()

        t = transform_pb2.Transformation(StringOut=self.localData.StringIn, StringListOut=self.localData.StringListIn, Error=self.localData.Error)
        return t
    
    def _amList(self):
        return self.localData.InputFormat == 0

    def _amString(self):
        return self.localData.InputFormat == 1
    
    def _wantString(self):
        return self.localData.OutputFormat == 1

    def _wantList(self):
        return self.localData.OutputFormat == 0

    def _setString(self):
        self.localData.InputFormat = 1

    def _setList(self):
        self.localData.InputFormat = 0

    def _inputToString(self):
        if self._amList(): 
            self.localData.StringIn = toString(self.localData.StringListIn)
            self.localData.StringListIn = []
            self._setString()

    def _inputToList(self):
        if self._amString():
            self.localData.StringListIn = toList(self.localData.StringIn)
            self.localData.StringIn = ""
            self._setList()

    def _replaceAcronyms(self):
        # this replaces the init call
        if not config.AcronymDictionaryLoaded:
            af = os.path.join(scriptDir, config.conf["File_Settings"]["SLANG_DICTIONARY"])
            config.AcronymDictionaryLoaded = acronyms.Load(af)
        self._inputToList()
        self.localData.StringListIn = acronyms.TranslateAcromymsFromList(self.localData.StringListIn)
    
    def _spacyInit(self):
        if not config.spacy_nlp:
            with spacy_lock:
                if not config.spacy_nlp:
                    print("Loading Spacy Model....")
                    config.spacy_nlp = spacy.load(config.conf["Server_Settings"]["SPACY_MODEL"])
                    print("Loaded Spacy")

    def _spacySplit(self):
        self._spacyInit()
        self._inputToString()
        if self._amString():
            self.localData.StringListIn = list(map(str,config.spacy_nlp(self.localData.StringIn)))
            self.localData.StringIn = ""
            self._setList()

    def _spacyLemmatize(self):
        self._spacyInit()
        self._inputToString()
        if self._amString():
            self.localData.StringListIn = [token.lemma_ if token.lemma_ != '-PRON-' else token.text for token in config.spacy_nlp(self.localData.StringIn) ]
            self.localData.StringIn = ""
            self._setList()

    def _spacyNER(self):
        self._spacyInit()
        self._inputToString()
        if self._amString():
            self.localData.StringListIn = [str(ent.label_) + '|' + str(ent.text) for ent in config.spacy_nlp(self.localData.StringIn).ents]
            self.localData.StringIn = ""
            self._setList()
        
    def _viperInit(self):
        #initial check prevents lock contention after load
        if not config.viperDict:
            with viper_lock:
                if not config.viperDict:
                    print("Loading Viper")
                    config.viperProbability = config.conf["File_Settings"]["VIPER_PROBABILITY"]
                    with open(config.conf["File_Settings"]["VIPER_DICTIONARY"]) as f:
                        config.viperDict = json.load(f)
                    print("Loaded Viper")

    def _viperString(self, s):
        result = list(s)
        for i, character in enumerate(s):
            if character not in config.viperDict:
                continue
            r = random.random()
            if r < config.viperProbability:
                result[i] = random.choice(config.viperDict[character])
        return "".join(result)
            
    def _viper(self):
        self._viperInit()
        if self._amString():
            self.localData.StringIn = self._viperString(self.localData.StringIn)
        elif self._amList():
            for i, s in enumerate(self.localData.StringListIn):
                self.localData.StringListIn[i] = self._viperString(s)

    def _spark_init(self):
        if not config.spark_init:
            with spark_lock:
                if not config.spark_init:
                    print("Loading Spark")
                    sparknlp.start()
                    config.spark_init = True
                    print("Spark Loaded")

    def _spark_sentiment_init(self):
        self._spark_init()
        if not config.spark_sentiment:
            with spark_sentiment_lock:
                if not config.spark_sentiment:
                    print("Loading Sentiment Pipeline")
                    config.spark_sentiment = PretrainedPipeline("analyze_sentiment_ml", "en")
                    print("Loaded Sentiment Pipeline")
     
    def _spark_sentiment(self):
        self._spark_sentiment_init()
        result = config.spark_sentiment.annotate(self.localData.StringIn)
        if not self._amString():
            self.localData.Error = "String Input Required for Spark Sentiment"
            return
        self.localData.StringListIn = list(map(str, result['sentiment']))
        self._setList()

    def _spark_spelling_init(self):
        self._spark_init()
        if not config.spark_spelling:
            with spark_spelling_lock:
                if not config.spark_spelling:
                    print("Loading Spelling Pipeline")
                    config.spark_spelling = PretrainedPipeline("spell_check_ml", "en")
                    print("Loaded Spelling Pipeline")

    def _spark_spelling(self):
        self._spark_spelling_init()
        if self._amList():
            tmp = list(map(str, map(lambda s: config.spark_spelling.annotate(s)["spell"], self.localData.StringListIn)))
            self.localData.StringListIn = tmp 
        elif self.localData.InputFormat == 1:
            self.localData.StringIn = " ".join(config.spark_spelling.annotate(self.localData.StringIn)["spell"])
    
    def _textBlobSpelling(self):
        if self._amString():
            tb = TextBlob(self.localData.StringIn)
            self.localData.StringIn = tb.correct()
        elif self._amList():
            tmp = [str(Word(x).spellcheck()[0]) for x in self.localData.StringListIn]
            self.localData.StringListIn = tmp

    def _textBlobSentiment(self):
        self._inputToString()
        self.localData.StringIn = str(TextBlob(self.localData.StringIn).sentiment)

    def _textBlobLemmatize(self):
        if self._amString():
            tb = TextBlob(self.localData.StringIn)
            self.localData.StringListIn = [x.lemmatize() for x in tb.words]
            self._setList()
        elif self._amList():
            tmp = [Word(word).lemmatize() for word in self.localData.StringListIn]
            self.localData.StringListIn = tmp 

    def _textBlobPOSTagging(self):
        self._inputToString()
        tb = TextBlob(self.localData.StringIn)
        self.localData.StringIn = str(tb.tags)

    def _textBlobSingularize(self):
        if self._amString():
            tb = TextBlob(self.localData.StringIn)
            self.localData.StringListIn = [x.singularize() for x in tb.words]
            self._setList()
        elif self._amList():
            tmp = [Word(word).singularize() for word in self.localData.StringListIn]
            self.localData.StringListIn = tmp 

def serve():
    try:
        global scriptDir
        config_file = os.path.join(scriptDir, '../config.yaml')
        config.conf = config.Load(config_file)
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=config.conf["Server_Settings"]["NUMBER_OF_WORKERS"]))
        transform_pb2_grpc.add_TransformerServicer_to_server(Transformer(), server)

        server.add_insecure_port('[::]:'+str(config.conf["Server_Settings"]["PORT"]))
        server.start()

        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)

    except KeyboardInterrupt:
        #cleanup, cleanup... everybody clean up...
        server.stop(0)

if __name__ == '__main__':
    #  logging.basicConfig()
    serve()

