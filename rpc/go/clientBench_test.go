package main
import (
    "testing"
    "gitlab.com/qubert/textCleaner/rpc/go/translate"
    "fmt"
)

const basePhrase = "I wish I were an Oscar Mayer Weiner! That is what I truly wish to be. Cause if I were a oscar mayer weiner, Everyone would be in lovvvveee... Oh everyone would be in looooove, Everyone would be in love with me"

func init() {
    //preload the call -- This loads up Spacy or whatever pipeline before the benchmarks
    translate.StringToList(basePhrase, []int32{translate.UPPER, translate.ACRONYMS, translate.SPARK_SPELLING, translate.SPARK_SENTIMENT, translate.SPACY_NER, translate.TEXT_BLOB_SPELLING, translate.TEXT_BLOB_SINGULARIZE, translate.TEXT_BLOB_LEMMATIZE, translate.TEXT_BLOB_POS_TAGGING, translate.TEXT_BLOB_SENTIMENT})
}

func runIt(b *testing.B, f func() ([]string, error)) {
    b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
                f()
			}
		})
        b.StopTimer()
        fmt.Println(f())
        b.StartTimer()
}

func BenchmarkSimpleSplit(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TO_LIST})})
}

func BenchmarkSpacySplit(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.SPACY_SPLIT})})
}

func BenchmarkSpacyLemmatize(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.SPACY_LEMMATIZE})})
}

func BenchmarkAcronyms(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.ACRONYMS})})
}

func BenchmarkSpacy_NER(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.SPACY_NER})})
}

func BenchmarkSparkSentiment(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.SPARK_SENTIMENT})})
}
func BenchmarkSparkSpelling(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.SPARK_SPELLING})})
}

func BenchmarkTextBlobSpelling(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TEXT_BLOB_SPELLING})})
}
func BenchmarkTextBlobSingularize(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TEXT_BLOB_SINGULARIZE})})
}
func BenchmarkTextBlobLemmatize(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TEXT_BLOB_LEMMATIZE})})
}
func BenchmarkTextBlobPOSTagging(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TEXT_BLOB_POS_TAGGING})})
}
func BenchmarkTextBlobSentiment(b *testing.B) {
    runIt(b, func() ([]string, error){return translate.StringToList(basePhrase, []int32{translate.TEXT_BLOB_SENTIMENT})})
}
