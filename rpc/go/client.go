package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/qubert/textCleaner/rpc/go/translate"
	"golang.org/x/sync/syncmap"
	"gopkg.in/yaml.v3"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"time"
)

var linkRe = regexp.MustCompile(`(?m)((f|ht)tp(s?)://(.*)[\S]+)|(\[.+\]\(.+\))`)

func testInterface(sid string, response interface{}) {
	if s, ok := response.(string); ok {
		if !strings.Contains(s, sid) {
			fmt.Println("Expected:", sid, "Response Failed:", response)
			panic("Assertion failure on return")
		}
	} else if l, ok := response.([]string); ok {
		if sid != l[0] {
			fmt.Println("Expected:", sid, "Response Failed:", response)
			panic("Assertion failure on return")
		}
	}
}

func timeTrack(start time.Time, description string, timeTracker *syncmap.Map) {
	val, ok := timeTracker.Load(description)
	if ok {
		timeTracker.Store(description, val.(int64)+(time.Since(start).Nanoseconds()))
	} else {
		timeTracker.Store(description, time.Since(start).Nanoseconds())
	}

	fmt.Println(description, "took", time.Since(start))
}

func reportTime(f func(), description string, timeTracker *syncmap.Map) {
	defer timeTrack(time.Now(), description, timeTracker)
	f()
}

func worker(sChan chan map[string]interface{}) {
	for s := range sChan {
		s["linksRemoved"] = linkRe.ReplaceAllString(s["body"].(string), "LINK_WAS_HERE")
		s["transformed"], _ = translate.StringToList(s["linksRemoved"].(string), []int32{translate.ACRONYMS, translate.SPACY_LEMMATIZE})
		s["NERs"], _ = translate.StringToList(s["body"].(string), []int32{translate.SPACY_NER})
		s["Spark_Sentiment"], _ = translate.StringToList(s["body"].(string), []int32{translate.SPARK_SENTIMENT})
		s["TextBlob_Sentiment"], _ = translate.StringToList(s["body"].(string), []int32{translate.TEXT_BLOB_SENTIMENT})
		j, _ := json.Marshal(s)
		fmt.Println(string(j))
	}
}

func main() {
	conf := make(map[string]map[string]interface{})
	yamlFile, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		panic("config file error: %s" + err.Error())
	}
	err = yaml.Unmarshal(yamlFile, conf)
	if err != nil {
		panic("yaml config decode error: %s" + err.Error())
	}
	numWorkers := conf["Client_Settings"]["NUMBER_OF_WORKERS"].(int)
	bufferSize := conf["Client_Settings"]["BUFFER_SIZE"].(int)

	sChan := make(chan map[string]interface{}, bufferSize)
	for x := 0; x < numWorkers; x++ {
		go worker(sChan)
	}
	f, err := os.Open(conf["Client_Settings"]["INPUT_FILE"].(string))

	if err != nil {
		fmt.Println("Error opening input file: ", err)
		os.Exit(2)
	}

	defer f.Close()
	d := json.NewDecoder(f)
	for {
		var dat map[string]interface{}
		err := d.Decode(&dat)
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		sChan <- dat
	}
}
