package translate

// NOTE The GRPC server must be running, or TESTS WILL FAIL

import (
	"fmt"
	"testing"
)

func Test_translate(t *testing.T) {
	for x := 110; x < 120; x++ {
		fmt.Println("string  IN:" + string(x))
		fmt.Println("String out first:", Translate(string(x), 0))
		fmt.Println("String out second:", Translate(string(x), 1))
	}
}
