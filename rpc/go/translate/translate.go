package translate

import (
	"context"
	"errors"
	pb "gitlab.com/qubert/textCleaner/rpc/proto"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"time"
    "path/filepath"
    "os"
)

// TODO DEFINE THESE IN THE PROTO....
const (
	UPPER           = 0
	LOWER           = 1
	TO_LIST         = 2
	TO_STRING       = 3
	ACRONYMS        = 4
	SPACY_SPLIT     = 5
	SPACY_LEMMATIZE = 6
	VIPER           = 7
	SPARK_SENTIMENT = 8
	SPARK_SPELLING  = 9
	SPACY_NER       = 10
    TEXT_BLOB_SENTIMENT = 11
    TEXT_BLOB_POS_TAGGING = 12
    TEXT_BLOB_SPELLING = 13
    TEXT_BLOB_LEMMATIZE = 14
    TEXT_BLOB_SINGULARIZE = 15
)

var address string

var numConnections int = 4

var requestLine chan *rpcRequest

type rpcRequest struct {
	request    *pb.Input
	resultLine chan *pb.Transformation
}

func init() {
	// read in the config details
	conf := make(map[string]map[string]interface{})
    goPath := os.Getenv("GOPATH")
    configPath := filepath.Join(goPath, "src", "gitlab.com","qubert","textCleaner","rpc", "config.yaml")
	yamlFile, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Printf("Error opening file %s\n", err)
	}

	requestLine = make(chan *rpcRequest, 100)

	err = yaml.Unmarshal(yamlFile, conf)
	if err != nil {
		log.Printf("yaml decode error: %s", err)
	}

	address = conf["Client_Settings"]["SERVER_ADDRESS"].(string)
	numConnections = conf["Client_Settings"]["NUMBER_OF_CONNECTIONS"].(int)

	for x := 0; x < numConnections; x++ {
		go handleTransformationRequests(newClientWithConn())
	}
}

func handleTransformationRequests(client *pb.TransformerClient) {
	for req := range requestLine {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*300)
		defer cancel()
		//TODO this should be in a destructor either on eintr or the like....
		// defer conn.Close()
		r, err := (*client).Transform(ctx, req.request)
		if err != nil {
			log.Fatalf("RPC Failed: %v", err)
		}
		req.resultLine <- r
	}
}

func newClientWithConn() *pb.TransformerClient {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	nc := pb.NewTransformerClient(conn)
	return &nc
}

func Must(val interface{}, err error) interface{} {
	if err != nil {
		panic(err)
	}
	return val
}

func sendRPC(input *pb.Input) (*pb.Transformation, error) {
	req := &rpcRequest{input, make(chan *pb.Transformation)}
	requestLine <- req
	response := <-req.resultLine

	var err error = nil
	if response.Error != "" {
		err = errors.New(response.Error)
	}

	return response, err
}

func ListToString(s []string, transformations []int32) (string, error) {
	RPC_Object := &pb.Input{InputFormat: pb.Input_LIST, OutputFormat: pb.Input_STRING, Transformations: transformations, StringIn: "", StringListIn: s}
	response, err := sendRPC(RPC_Object)
	return response.StringOut, err
}

func ListToList(s []string, transformations []int32) ([]string, error) {
	RPC_Object := &pb.Input{InputFormat: pb.Input_LIST, OutputFormat: pb.Input_LIST, Transformations: transformations, StringIn: "", StringListIn: s}
	response, err := sendRPC(RPC_Object)
	return response.StringListOut, err
}
func StringToList(s string, transformations []int32) ([]string, error) {
	RPC_Object := &pb.Input{InputFormat: pb.Input_STRING, OutputFormat: pb.Input_LIST, Transformations: transformations, StringIn: s, StringListIn: []string{}}
	response, err := sendRPC(RPC_Object)
	return response.StringListOut, err
}

func StringToString(s string, transformations []int32) (string, error) {
	RPC_Object := &pb.Input{InputFormat: pb.Input_STRING, OutputFormat: pb.Input_STRING, Transformations: transformations, StringIn: s, StringListIn: []string{}}
	response, err := sendRPC(RPC_Object)
	return response.StringOut, err
}
